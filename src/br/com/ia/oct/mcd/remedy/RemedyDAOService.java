package br.com.ia.oct.mcd.remedy;

import java.util.HashMap;
import java.util.List;

public interface RemedyDAOService {
	public String  createEntry 										(String formName, HashMap<Integer, Object> object) throws Exception;
	public boolean updateEntry 										(String formName, String requestId, HashMap<String, Object> object) throws Exception;
	public boolean deleteEntry 										(String formName, String requestId);
	public List<HashMap<String, String>>  queryEntry  				(String formName, String query, HashMap<Integer, String> campos) throws Exception;
	public List<HashMap<String, String>>  queryEntry  				(String formName, String query, HashMap<Integer, String> campos, Integer limit) throws Exception;
	public List<String>  formList  									() throws Exception;
	public String getEntryFieldValue								(String formName, String requestId, int fieldId) throws Exception;
	public HashMap<Integer, String> getAllFieldNames				(String formName) throws Exception;
	public HashMap<Integer, List<Object>> getAllFieldNamesWithDetails (String formName) throws Exception;
	public HashMap<Integer, Object> convertObjectToHash 			(Object obj) throws Exception;
	public HashMap<String, Object> convertObjectToHashStringKey  	(Object obj) throws Exception;
	public String getFormName										(Object obj) throws Exception;
}
