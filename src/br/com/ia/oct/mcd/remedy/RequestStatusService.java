package br.com.ia.oct.mcd.remedy;

import br.com.ia.oct.mcd.common.entity.remedy.RequestStatus;

public interface RequestStatusService {
	public RequestStatus getStatusIN (String companyIN, String companyOUT, String statusIN) throws Exception;
	public RequestStatus getStatusOUT (String companyIN, String companyOUT, String statusOUT) throws Exception;
}
