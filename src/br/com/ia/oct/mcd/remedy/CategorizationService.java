package br.com.ia.oct.mcd.remedy;

import javax.ejb.Remote;
import br.com.ia.oct.mcd.common.monitoring.OCTComponent;

 
@Remote
public interface CategorizationService extends OCTComponent {
	//public Categorization getCategorization (String tipo, String companyIN, String companyOUT, String categoryIN) throws Exception;
	public String[] getMap(String p_tipo_req, String p_map, String p_sentido, String p_de_1, String p_de_2, String p_de_3, String p_de_4);
	public String[] getMapMcD (String p_map, String p_para_1, String p_para_2, String p_para_3, String p_para_4, Boolean pBuscarDefault);
}
