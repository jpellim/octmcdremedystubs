package br.com.ia.oct.mcd.remedy;

import java.util.HashMap;
import java.util.List;

public interface RemedyBOService {
	public String createWorkInfo(String Notes, String incidentID) throws Exception;
	public String createIncident(String summary, String notes, String company, String tier1, String tier2, String tier3, String impact, String urgency, String statusOUT, String serviceType, String number) throws Exception;
	public String sayHello();
	public String createEntry(String formName, HashMap<Integer, Object> object) throws Exception;
	public List<HashMap<String, String>> frmQuery(String p_frm, String p_qry);
	public List<HashMap<String, String>> frmQuery(String p_frm, String p_qry, HashMap<Integer, String> fields);
	public boolean updateEntry(String formName, String requestId, HashMap<String, Object> object) throws Exception;
	public HashMap<Integer, String> getAllFieldNames(String formName) throws Exception ;
}
